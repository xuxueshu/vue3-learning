## vue3 Setup 语法糖

`<script setup lang="ts" name='xxx'></script> `

该语法中的name需要vue插件支持

npm install vite-plugin-vue-setup-extend -D

在vite.config.ts中注册

```tsx
import xxx from 'vite-plugin-vue-setup-extend'

export default defineConfig({
    plugins:[
        xxx()
    ]
})
```

安装完成后，重启项目

## 响应式数据ref创建基本类型对象

ref是个函数。

```tsx
<div>
    {{xxxx}}
</div>

import {ref} from 'vue'

let xxxx = ref('ceshi')
//ts/js中使用要加xxx.value
const test =()=>{
    xxxx.value
}
```



## 响应式数据reactive创建对象类型的数据

```tsx
<div>
	<h1> 11 {{car.brand}} 22 {{car.price}} </h1>
</div>
import { reactive } from " vue "
//深层次并只能定义对象类型

let car = reactive({brand:'福特',price:'100'})
let games = reactive([{id:'aysdyfsatr01',name:'王者荣耀'}])

```



## ref创建对象类型的响应式数据

```tsx
<div></div>
import {ref} from 'vue'
//原理上还是reactive
let car = ref({brand:'福特',price:'100'})
let games = ref([{id:'aysdyfsatr01',name:'王者荣耀'}])
```

## ref和reactive的区别

ref创建变量必须使用value（可以使用Vue-Official插件，省略.value)

<img src="image\image-20240325141101779.png" alt="image-20240325141101779" />

<img src="image\image-20240325141213346.png" alt="image-20240325141213346" />

![image-20240325141244230](image\image-20240325141244230.png)

reactive 重新分配一个新对象，会失去响应式(可以使用 0bject.assign 去整体替换)

```js
let car =reactive({brand:'牌子',price:100})
//如果实在想使用reactive进行响应式，而且想修改中的值
function test(){
	Object.assgin(car,{brand:'欧拓',price:1})
    //将后者的属性全部添加到car(第一个内)
}
```



### 使用原则:

1.若需要一个基本类型的响应式数据，必须使用ref。
2.若需要一个响应式对象，层级不深，ref、reactive 都可以。
3.若需要一个响应式对象，且层级较深，推荐使用reactive。

## toRefs 和 toRef

```tsx
import {reactive, toRefs } from 'vue'
let person = reactive({name:'xxx',age:10})
//将reactive定义的对象变成一个一个ref
let {name,age} = toRefs(person)
name.value='text'
age.value=18
//toRefs是全部取出来，toRef是取出单个来
```

## computed计算属性

```tsx
import {computed} from 'vue'
//computed是有缓存的,且一下写法为只读写法不能修改
let fullName = computed(()=>{
	return firstName.value.slice(0,1).toUppercase()+firstName.value.slice(1)+'-'+ lastName.value
})//输出结果Zhang—san
//以下写法是可读可修改的
let fullName = computed({
    get(){
        return firstName.value.slice(0,1).toUppercase()+firstName.value.slice(1)+'-'+ lastName.value
    },
    set(val){
        const [str1,str2] = val.split('-')//数组也能解构赋值，val数值为'li-si'
        fristName.value = str1
        lastName.value = str2
    }
})
```

## watch(监听属性)

只会监听以下4中属性

1. ref定义的数据
2. reactive定义的数据
3. 函数返回一个值
4. 一个包含上述内容的数组

### 情况1

监视 ref 定义的【基本类型】数据:直接写数据名即可，监视的是其 value 值的改变

watch(数据，方法)

```tsx
import {watch} from 'vue'

watch(num,(newValue,oldValue)=>{
    if(newValue >= 10){
        stopWatch()//停止监听
    }
})
```

### 情况2

监视 ref 定义的【对象类型】数据:直接写数据名，监视的是对象的【地址值】，若想监视对象内部的数据，要手动开启深度监视。

```tsx
import {watch} from 'vue'
let person = ref({
    name:'xxx',
    age:18
})
watch(person,(newValue,oldValue)=>{
},{
    deep:true,//开启深度监视
    immediate:true//立即开启监视，渲染的时候就开始监视person
})
//watch的第一个参数是:被监视的数据
//watch的第二个参数是:监视的回调
//watch的第三个参数是:配置对象(deep,immediate等...)
```

#### 注意：

若修改的是ref 定义的对象中的属性，newValue 和 oldvalue 都是新值，因为它们是同一个对象。
若修改整个 ref 定义的对象，newValue 是新值， oldValue 是旧值，因为不是同一个对象了。

### 情况3

监视 reactive 定义的【对象类型】数据，且默认开启了深度监视。并且无法关闭

```tsx
import {watch} from 'vue'

let test = reactive({
    name:'xxx',
    age:18
})

watch(test,(newValue,oldValue)=>{
    console.log( 'test值变化了',newValue,oldValue)
})

```

### 情况4

监视 ref 或 reactive 定义的【对象类型】数据中的某个属性，注意点如下：
1.若该属性值不是【对象类型】，需要写成函数形式。
2.若该属性值是依然是【对象类型】，可直接编，也可写成函数，不过建议写成函数。

```tsx
import {watch} from 'vue'

let test = reactive({
    name:'xxx',
    age:18,
    car:{
        car1:'宝马'，
        car2：'奔驰'
    }
})
// 监视，情况四:监视响应式对象中的某个属性，且该属性时基本类型的，要写成函数式
watch(()=> test.name ,(newValue,oldValue)=>{
    console.log( 'test.name值变化了',newValue,oldValue)
})
watch(()=> test.car ,(newValue,oldValue)=>{
    console.log( 'test.car值变化了',newValue,oldValue)
}，{
      deep:true
      })
```

结论:监视的要是对象里的属性，那么最好写函数式，注意点:若是对象监视的是地址值，需要关注对象内部，需
要手动开启深度监视。

### 情况5

```tsx
// 监视，情况五:监视上述的多个数据
watch([()=>test.name,test.car],(newValue,oldValue)=>{
    console.log('person.car变化了'newValue,oldValue)
}{deep:true})
```

## watchEffect(结果影响)

```tsx
import { watchEffect } from 'vue'
watchEffect(()=>{
    if(test.value>=60||height.value>=80){
        console.log('给服务器发请求')
    }
})
```

## 标签的ref属性

```tsx
<h2 ref='test'>测试</h2>
import {ref} from 'vue'
let test = ref()
console.log(test.value)//打印结果<h2>测试</h2>
```

## ts使用的，接口，泛型，自定义类型

```tsx
//定义一个接口，用于限制person对象的具体属性
export interface PersonInter{
    id:string,//使用小写
	name:string,
	age:number
}

// 一个自定义类型
// export type Persons = Array<PersonInter>
export type Persons = PersonInter[]
//使用
import { type Persons} from '定义ts泛型的文件路径'
let test:Persons =[
    {id:'1111111',name:'测试'，age:10}
]
```

## props的使用

```tsx
import {reactive}from 'vue'

import {type Persons} from '@/types'

let personList =reactive<Persons>([
    {id:'asudfysafd01',name:'张三',age:18},
    {id:'asudfysafd02',name:'李四',age:20},
    {id:'asudfysaf)d03',name:'王五',age:22}
1)
```

```tsx
//父
<组件名 a='哈哈哈'></组件名>
//子
import {defineProps} from 'vue'
//接收使用
defineProps(['a'])
//接收使用，同时使用
let x = defineProps(['a'])
```

```tsx
import {defineProps} from 'vue'
import {type Persons} from '@/types'
// 只接收list1
defineProps(['list'])
//接收list+限制类型
defineProps<{list:Persons}>()
```

```tsx
import {defineProps,withDefaults} from 'vue'
//接收list + 限制类型 + 限制必要性 
defineProps<{list?:Persons}>()
//接收list + 限制类型 + 限制必要性+ 指定默认值
withDefaults(defineProps<{list?:Persons}>(),{
    list:()=>[{
        id:'111111',
        name:'test',
        age:18
    }]
})
```

## 自定义Hooks

